package youtube;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.qameta.allure.Step;
import io.qameta.htmlelements.WebPageFactory;
import io.qameta.htmlelements.element.HtmlElement;
import listener.AllureListener;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObject.commonGoogle.logInPage.GoogleLogInPage;
import pageObject.commonGoogle.registrationPage.InputField;
import pageObject.commonGoogle.registrationPage.RegistrationPage;
import pageObject.youTubePage.YouTubePage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;
import static io.qameta.htmlelements.matcher.HasTextMatcher.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static utils.matchers.CaseInsensitiveSubstringMatcher.containsIgnoringCase;

@DisplayName("Проверки формы регистрации гугл")
public class RegistrationTest {
    private static WebDriver driver;
    private static YouTubePage youTubePage;
    private static GoogleLogInPage googleLogInPage;
    private static RegistrationPage registrationPage;


    @BeforeAll
    public static void before() {
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        WebPageFactory factory = new WebPageFactory().listener(new AllureListener());
        youTubePage = factory.get(driver, YouTubePage.class);
        googleLogInPage = factory.get(driver, GoogleLogInPage.class);
        registrationPage = factory.get(driver, RegistrationPage.class);
        goGoPowerRangers();
    }

    @AfterAll
    public static void after() {
        if (driver != null) {
            driver.quit();
        }
    }

    private Consumer<InputField> sendKeys(String value) {
        return x -> x.waitUntil("", displayed(), 10).sendKeys(value);
    }

    @Step("Заполянем каждое поле регистрационной формы")
    //это нужно, т.к. ошибки заполнения некоторых полей не дают посмотреть ошибки конкрутного поля
    private void fillForm(RegistrationPage registrationPage, String value) {
        new ArrayList<String>() {{
            add("Имя");
            add("Фамилия");
            add("Адрес электронной почты");
            add("Пароль");
            add("Подтвердите пароль");
        }}.stream().map(registrationPage::inputField).collect(Collectors.toList()).forEach(x -> {
            x.inputField().clear();
            x.inputField().sendKeys(value);
        });
    }


    @Step("Переход на страницу регистрации")
    private static void goGoPowerRangers() {
        youTubePage.go();
        youTubePage.searchForm().searhArrow().waitUntil("", displayed(), 10);
        youTubePage.logInButton().click();
        googleLogInPage.createAccountButton().waitUntil("", displayed(), 10).click();
    }


    @Test
    @DisplayName("Проверка полей регистрационной формы на ввод некорректных данных в целом. Регистрация не прошла")
    public void checkInvalidDataForRegistrationFormFields() {
        fillForm(registrationPage, "~~~~~~~~`````");
        registrationPage.nextButton().click();
        registrationPage.inputField("Имя").should(displayed());
    }


    @ParameterizedTest(name = "Негативные проверки №{index} для поля ввода [{arguments}]")
    @ValueSource(strings = {"Имя", "Фамилия"})
    @DisplayName("Негативные проверка полей")
    public void checkNameAndFamilyInputFields(String name) {
        //поскольку валидация формы срабатывает только после нажатия кнопки \"Далее\"
        //нам придётся её заполнить всю сразу, а только потом по одному полю проверять реакцию формы
        InputField htmlElement = registrationPage.inputField(name);
        new ArrayList<String>() {{
            add("");
            add("      ");
        }}.forEach(it -> {
            fillForm(registrationPage, "qwerty1234");
            htmlElement.inputField().clear();
            htmlElement.inputField().sendKeys(it);
            registrationPage.nextButton().click();
            htmlElement.error().should(hasText(containsIgnoringCase("Укажите " + name.substring(0, name.length() - 2))));
        });
        fillForm(registrationPage, "qwerty1234");
        htmlElement.inputField().sendKeys("~");
        registrationPage.nextButton().click();

        assertThat("Нет сообщения \"Проверьте правильность своих имени и фамилии.\"",
                registrationPage.findElement(By.xpath("//*[text()='Проверьте правильность своих имени и фамилии.']")).isDisplayed());
    }

    @Test
    @DisplayName("Негативные проверки поля ввода \"Адрес электронной почты\"")
    public void checkEmailInputField() {
        //поскольку валидация формы срабатывает только после нажатия кнопки \"Далее\"
        //нам придётся её заполнить всю сразу, а только потом по одному полю проверять реакцию формы
        InputField htmlElement = registrationPage.inputField("Адрес электронной почты");
        new HashMap<String, String>() {{
            put("", "Укажите адрес Gmail");
            put("111111", "Не забудьте указать символ \"@\".");
            put("111@gmail@com", "Укажите адрес электронной почты, который содержит только один символ \"@\".");
            put("111@gmail.com.", "Это имя пользователя уже занято. Попробуйте другое.");
            put("БЮ@gmail.com.", "Можно использовать только буквы латинского алфавита, цифры и некоторые специальные символы.");
            put("John..Doe@example.com", "Вы не можете создать аккаунт с этим доменным именем: example.com.");
            put("\"John..Doe\"@host.com", "Можно использовать только буквы латинского алфавита, цифры и некоторые специальные символы.");
        }}.forEach((key, value) -> {
            fillForm(registrationPage, "qwerty1234");
            htmlElement.inputField().clear();
            htmlElement.inputField().sendKeys(key);
            registrationPage.nextButton().click();
            htmlElement.error().should(hasText(value));
        });
    }

    @Test
    @DisplayName("Негативные проверка полей установки пароля")
    public void checkNameAndFamilyInputFields1() {
        InputField password = registrationPage.inputField("Пароль");
        InputField passwordRepeat = registrationPage.inputField("Подтвердите пароль");
        List<InputField> list = new ArrayList<InputField>() {{
            add(password);
            add(passwordRepeat);
        }};
        new HashMap<String, String>() {{
            put("12345", "Пароль не может быть короче 8 символов");
            put("", "Введите пароль");
            put("\"aa,dб asd.\"", "Разрешены только буквы, цифры и общие символы пунктуации.");
            put("12345678", "Пароль не достаточно надежный. Попробуйте сочетание букв, цифр и символов.");
            put("password", "Пароль не достаточно надежный. Попробуйте сочетание букв, цифр и символов.");
            put("qwertyqwerty", "Пароль не достаточно надежный. Попробуйте сочетание букв, цифр и символов.");
            put("12345678", "Пароль не достаточно надежный. Попробуйте сочетание букв, цифр и символов.");
        }}.forEach((key, value) -> {
            fillForm(registrationPage, "qwerty1234");
            list.forEach(x -> {
                x.inputField().clear();
                x.inputField().sendKeys(key);
            });
            registrationPage.nextButton().click();
            password.error().should(hasText(value));
        });
    }
}
