package youtube;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.qameta.htmlelements.WebPageFactory;
import io.qameta.htmlelements.annotation.Description;
import listener.AllureListener;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObject.youTubePage.YouTubePage;
import pageObject.youTubePage.SearchResult;

import static io.qameta.htmlelements.matcher.DisplayedMatcher.displayed;
import static io.qameta.htmlelements.matcher.HasTextMatcher.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DisplayName("Проверки поиска видео в youtube")
public class SearchVideoTest {
    private static WebPageFactory factory;
    private static WebDriver driver;
    private static YouTubePage youTubePage;


    @BeforeAll
    public static void before() {
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        factory = new WebPageFactory().listener(new AllureListener());
        youTubePage = factory.get(driver, YouTubePage.class);
    }

    @AfterAll
    public static void after(){
        if (driver != null){
            driver.quit();
        }
    }

    @Test()
    @DisplayName("Поисковый запрос без результатов поиска")
    public void emptyResultsTest() {
        youTubePage.go();
        youTubePage.searchForm().searhArrow().waitUntil("", displayed(), 10)
                .sendKeys("~~~~~~~~~~~~~");
        String value = youTubePage.searchForm().searhArrow().getAttribute("value");
        assertThat(value, is("~~~~~~~~~~~~~"));
        youTubePage.searchForm().searchButton().click();
        youTubePage.searchResults().should(IsEmptyCollection.<SearchResult>empty());
        youTubePage.resultsNotFound().should(hasText("Результатов не найдено"));
    }

    @Test()
    @DisplayName("Поисковый запрос не работает с пустой строкой")
    public void emptySearchArrowTest() {
        youTubePage.go();
        youTubePage.searchForm().searhArrow().waitUntil("", displayed(), 10)
                .sendKeys("PropellerAdsQAMeetup - February 2018");
        String value = youTubePage.searchForm().searhArrow().getAttribute("value");
        assertThat(value, is("PropellerAdsQAMeetup - February 2018"));
        youTubePage.searchForm().searchButton().click();
        youTubePage.searchResults().get(0).should(hasText(containsString("PropellerAdsQAMeetup - February 2018")));
        youTubePage.searchForm().searhArrow().clear();
        youTubePage.searchForm().searchButton().click();
        youTubePage.searchResults().get(0).should(hasText(containsString("PropellerAdsQAMeetup - February 2018")));
    }

    @Test()
    @DisplayName("Поисковый запрос не изменяется после выполнения поиска")
    public void searchArrowTest() {
        youTubePage.go();
        String value = youTubePage.searchForm().searhArrow().getAttribute("value");
        assertThat(value, is(isEmptyString()));
        youTubePage.searchForm().searhArrow().waitUntil("", displayed(), 10)
                .sendKeys("PropellerAdsQAMeetup - February 2018");
        value = youTubePage.searchForm().searhArrow().getAttribute("value");
        assertThat(value, is("PropellerAdsQAMeetup - February 2018"));
        youTubePage.searchForm().searchButton().click();
        value = youTubePage.searchForm().searhArrow().getAttribute("value");
        assertThat(value, is("PropellerAdsQAMeetup - February 2018"));
    }

    @Test()
    @DisplayName("Поиск видео по точному совпадению")
    public void searchResultTest() {
        youTubePage.go();
        youTubePage.searchForm().searhArrow().waitUntil("", displayed(), 10);
        youTubePage.searchForm().searhArrow().sendKeys("allintitle:\"PropellerAdsQAMeetup - February 2018\"");
        youTubePage.searchForm().searchButton().click();
        youTubePage.searchResults().should(everyItem(hasText("PropellerAdsQAMeetup - February 2018")));
    }
}
