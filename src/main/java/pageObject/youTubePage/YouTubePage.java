package pageObject.youTubePage;

import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedList;
import io.qameta.htmlelements.element.HtmlElement;
import io.qameta.htmlelements.extension.page.BaseUrl;
import pageObject.youTubePage.header.WithHeader;


@BaseUrl("https://www.youtube.com/")
public interface YouTubePage extends WebPage, WithHeader {

    @FindBy("//div[@id='contents']//ytd-video-renderer")
    @Description("Список результатов поиска видео")
    ExtendedList<SearchResult> searchResults();

    @FindBy("//div[contains(@class,'promo-title')]")
    @Description("Результаты не найдены")
    HtmlElement resultsNotFound();
}

