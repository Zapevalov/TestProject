package pageObject.youTubePage;

import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface SearchResult extends ExtendedWebElement<SearchResult> {

    @FindBy(".//div[@id='title-wrapper']/h3")
    @Description("Название видео")
    HtmlElement titleWrapper();

}
