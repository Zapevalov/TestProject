package pageObject.youTubePage.header;

import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface YouTubeSearchForm extends ExtendedWebElement<YouTubeSearchForm> {
    @FindBy(".//input")
    @Description("Поисковая строка")
    HtmlElement searhArrow();

    @FindBy(".//button[@id='search-icon-legacy']")
    @Description("Кнопка \"Поиск\"")
    HtmlElement searchButton();
}
