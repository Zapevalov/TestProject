package pageObject.youTubePage.header;

import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.Link;

public interface WithHeader extends ExtendedWebElement<WithHeader> {

    @FindBy("//form[@id='search-form']")
    @Description("Поисковая форма")
    YouTubeSearchForm searchForm();

    @FindBy("//*[@id='masthead']//ytd-button-renderer[.//text()='Войти']")
    @Description("Кнопка \"Войти\"")
    Link logInButton();


}
