package pageObject.commonGoogle.registrationPage;

import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.ExtendedWebElement;
import io.qameta.htmlelements.element.HtmlElement;

public interface InputField extends ExtendedWebElement<InputField> {

    @FindBy(".//input")
    @Description("Поле ввода {{ name }}")
    HtmlElement inputField();

    @FindBy("./div[2]")
    @Description("Сообщение об ошибке ввода")
    HtmlElement error();
}
