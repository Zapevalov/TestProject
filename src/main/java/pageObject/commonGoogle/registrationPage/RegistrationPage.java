package pageObject.commonGoogle.registrationPage;

import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.annotation.Param;
import io.qameta.htmlelements.element.HtmlElement;

public interface RegistrationPage extends WebPage {

    @FindBy("//div[./div/div/div[./*[text()='{{ name }}']]]")
    @Description("Поле {{ name }}")
    InputField inputField(@Param("name") String name);

    @FindBy("//div[@id='accountDetailsNext']")
    @Description("Кнопка \"Далее\"")
    HtmlElement nextButton();
}
