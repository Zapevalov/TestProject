package pageObject.commonGoogle.logInPage;

import io.qameta.htmlelements.WebPage;
import io.qameta.htmlelements.annotation.Description;
import io.qameta.htmlelements.annotation.FindBy;
import io.qameta.htmlelements.element.HtmlElement;

public interface GoogleLogInPage extends WebPage {

    @FindBy("//*[text()='Создать аккаунт']")
    @Description("Кнопка \"Создать аккаунт\"")
    HtmlElement createAccountButton();
}
